/**
 * This module geenrate a OTP of 6 characters length
 */
var speakeasy = require("speakeasy");
var counter = 0;

var generateToken = function (secret) {
    var token = speakeasy.hotp({
        secret: secret.base32,
        encoding: 'base32',
        counter: counter
    });
    counter = counter + 1;
    return token;
};

var generateSecret = function (length) {
    secret = speakeasy.generateSecret({
        lenght: length
    });
    return generateToken(secret);
};

module.exports.getOTP = function (length) {
    return generateSecret(length);
}
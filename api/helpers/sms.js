const sms = require("msg91")(process.env.SMS_API_KEY, process.env.SMS_SENDER_ID, "4");

function sendTransactionalSMS(mobileNo, message) {
    sms.send(mobileNo, message, function (err, response) {
        if (err) {
            console.log('Error Sending Transactional SMS:', err);
            return false;
        } else {
            console.log('SMS Response:', response);
            return true;
        }
    });
}

module.exports = {
    send: sendTransactionalSMS
}
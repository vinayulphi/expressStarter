function missingBody(res, msg) {
    res.status(400).json({
        statusCode: 1,
        message: msg || 'Required Parameters Missing/Invalid Parameters'
    });
}

function errorResponse(res, msg) {
    res.status(501).json({
        statusCode: 2,
        success: false,
        message: msg || 'Server Error, Please Try Again Later'
    });
}

function successPostResponse(res, data, msg) {
    res.status(200).json({
        statusCode: 3,
        message: msg || 'Success',
        success: true,
        result: data
    });
}

function successGetResponse(res, data, msg) {
    res.status(200).json({
        statusCode: 3,
        message: msg || 'Success',
        success: false,
        result: data
    });
}

function noRecordsFound(res, msg) {
    res.status(200).json({
        statusCode: 4,
        message: msg || 'Unable to find required information',
        success: false,
        result: null
    });
}

function alreadyRegistered(res, msg) {
    res.status(200).json({
        statusCode: 5,
        message: msg || 'User Already Registered',
        success: false,
        result: null
    });
}

module.exports = {
    missingBody: missingBody,
    errorResponse: errorResponse,
    successPostResponse: successPostResponse,
    successGetResponse: successGetResponse,
    noRecordsFound: noRecordsFound,
    alreadyRegistered: alreadyRegistered
}
const AuthModel = require('./authentication.models');
const otp = require('../../helpers/otp');
const sms = require('../../helpers/sms');
const secret = process.env.SECRET;
const validators = require('../../helpers/validators');
let newOTP;
let otpMsg = '';

function signupUser(user, callback) {
    AuthModel.addUser(user, function (err, res) {
        if (err) {
            callback(err, null);
        } else if (res) {
            // Need to send OTP
            newOTP = otp.getOTP(6);
            otpMsg = 'Your User Verification OTP Is: ' + newOTP;
            AuthModel.setOTP(res.mobileNo, newOTP, function(err, res){
                // sms.send(res.mobileNo, otpMsg);
                callback(null, res);
            })
            
        } else {
            callback(null, null);
        }
    })
}

function verifyUser(mobileNo, otp, callback) {
    AuthModel.verifyOTP(mobileNo, otp, function(err, res){
        if(err) {
            callback(err, null);
        } else if(res) {
            callback(null, res);
        } else {
            callback(null, null);
        }
    })
}

function login(authString, callback) {
    validators.decodeAuthString(authString, function(mobileNo, password){
        if(mobileNo && password) {
            AuthModel.login(mobileNo, password, function(err, res){
                if (err) {
                    callback(err, null);
                } else if (res) {
                    callback(null, res);
                } else {
                    callback(null, null);
                }
            })
        } else {
            callback(null, null);
        }
    })
}

module.exports = {
    signUpUser: signupUser,
    verifyUser: verifyUser,
    login: login
}
const express = require('express');
const router = express.Router();
const AuthCtrl = require('./authentication.controller');
module.exports = router;

// Custom Helpers

const resp = require('./../../helpers/responseHelpers');
let headers = null;

router.post('/signup', function (req, res) {
    if (req.body && req.body.newUser) {

        AuthCtrl.signUpUser(req.body.newUser, function (err, docs) {
            if (err) {
                if (err.name && err.name === 'ValidationError') {
                    resp.errorResponse(res, 'Required Fields Are Missing');
                } else if (err.code && err.code === 11000) {
                    resp.alreadyRegistered(res, 'Mobile Number/Email Id Already Registered');
                } else {
                    resp.errorResponse(res, 'Internal Server Error');
                }

            } else if (docs) {
                resp.successPostResponse(res, 'Successfully Signed Up New User');
            }
        })
    } else {
        resp.missingBody(res, 'Missing Body');
    }
})

router.post('/verify', function (req, res) {
    if (req.body.mobileNo && req.body.otp) {
        AuthCtrl.verifyUser(req.body.mobileNo, req.body.otp, function (err, docs) {
            if (err) {
                resp.errorResponse(res);
            } else if (docs) {
                resp.successPostResponse(res, 'Verified Mobile No');
            } else {
                resp.noRecordsFound(res, 'No Mobile Number Found');
            }
        })
    } else {
        resp.missingBody(res, 'Missing MobileNo/OTP');
    }
})
router.post('/login', function (req, res) {
    if (req.headers && req.headers.authorization) {
        headers = req.get('authorization');
        headers = headers.split(' ');
        AuthCtrl.login(headers[1], function (err, docs) {
            if (err) {
                resp.errorResponse(res);
            } else if (docs) {
                resp.successPostResponse(res, docs, 'Authenticated');
            } else {
                resp.noRecordsFound(res, 'Invalid Mobile Number/Password');
            }
        })
    } else {
        resp.missingBody(res, 'Missing MobileNo/OTP');
    }
})
/**
 * All functions for mongoose model related to Users are done here
 */

const Users = require('../../models/users.model').UserModel;
const bcrypt = require('bcrypt');
const validators = require('../../helpers/validators');
let query = {};
let update = {};

function addUser(user, callback) {
    Users.create(user, (err, res) => {
        if (err) {
            console.log('UsersModel Error:', err);
            callback(err, null);
        } else if (res) {
            console.log('UsersModel Result:', res);
            callback(null, res);
        }
    })
}

function setOTP(mobileNo, otp, callback) {
    query = {
        mobileNo: mobileNo
    };
    update = {
        $set: {
            curOtp: otp
        }
    }
    Users.findOneAndUpdate(query, update, (err, res) => {
        if (err) {
            console.log('UsersModel Update Error:', err);
            callback(err, null);
        } else if (res) {
            console.log('UsersModel Update Result:', res);
            callback(null, res);
        }
    })
}

function verifyOTP(mobileNo, otp, callback) {
    query = {
        mobileNo: mobileNo,
        curOtp: otp
    };
    update = {
        $set: {
            curOtp: null,
            mobileNoVerified: true
        }
    }
    Users.findOneAndUpdate(query, update, (err, res) => {
        if (err) {
            console.log('UsersModel Update Error:', err);
            callback(err, null);
        } else if (res) {
            console.log('UsersModel Update Result:', res);
            callback(null, res);
        }
    })
}

function login(mobileNo, password, callback) {
    query = {
        mobileNo: mobileNo
    };
    Users.findOne(query, (err, res) => {
        if (err) {
            callback(err, null);
        } else if (res) {
            bcrypt.compare(password, res.password, (err, same) => {
                if (err) {
                    callback(err, null);
                } else if (same) {
                    // Passwords Verified Need to generate token
                    validators.generateJWTToken(res._id, callback);
                }
            })
        } else {
            callback(null, null);
        }
    })

}

function findUserById(id, callback) {
    let projection = {
        password: 0
    }
    Users.findById(id, projection, (err, res) => {
        if (err) {
            callback(err, null);
        } else {
            if (res) {
                callback(null, res);
            } else {
                callback(null, null);
            }
        }
    })
}

module.exports = {
    addUser: addUser,
    setOTP: setOTP,
    verifyOTP: verifyOTP,
    login: login,
    findUserById: findUserById
}
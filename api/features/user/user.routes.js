const express = require('express');
const router = express.Router();
const UserCtrl = require('./user.controller');
module.exports = router;
const resp = require('./../../helpers/responseHelpers');
router.get('/', (req, res) => {
    console.log('User Info:', req.user);
    
    resp.successPostResponse(res, req.user._doc, 'Success Auth');
})
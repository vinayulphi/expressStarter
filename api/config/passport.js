const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
// const User = require('../modules/authentication/login');
var mongojs = require('mongojs');
var db = mongojs(process.env.DB_URL, ['users', 'tempUsers']);
var ObjectId = require('mongodb').ObjectID;
const AuthModel = require('../features/authentication/authentication.models');
module.exports = function (passport) {
    var opts = {};
    // opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
    opts.secretOrKey = process.env.SECRET;

    passport.use(new JwtStrategy(opts, function (jwtPayload, done) {
        console.log('User Authenticating:', jwtPayload);
        AuthModel.findUserById(jwtPayload._id, function (err, doc) {
            if (err) {
                var errMsg = {
                    message: 'Unauthorized User For First Nanny',
                    err: err
                };
                return done(errMsg, false);
            } else if (doc) {
                return done(null, doc);
            } else {
                return done(null, false);
            }
        })

    }))
}
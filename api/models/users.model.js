// import {mongoose} from 'mongoose';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const validator = require('../helpers/validators');
const UserSchema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    fid: {
        type: String,
        required: false
    },
    gid: {
        type: String,
        required: false
    },
    updated_at: {
        type: Date,
        required: false
    },
    role: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        minlength: 4
    },
    mobileNo: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 11,
        unique: true
    },
    emailId: {
        type: String,
        unique: true,
        required: [
            true, ['Email Id Is Required']
        ],
        validate: {
            validator: function (email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            },
            message: 'Not a valid email id'
        },

    },
    mobileNoVerified: {
        type: Boolean,
        required: false
    },
    address: {
        lat: {
            type: mongoose.Schema.Types.String,
            required: false
        },
        lang: {
            type: mongoose.Schema.Types.String,
            required: false
        },
        fullAddress: {
            type: String,
            required: false
        },
        geoAddress: {
            type: String,
            required: false
        }
    },
    device: {
        uuid: {
            type: String,
            required: true
        },
        platform: {
            type: String,
            required: false
        },
        player_id: {
            type: String,
            required: false
        }
    },
    profile_image_url: {
        type: String,
        required: false
    },
    average_rating: {
        type: Number,
        required: false
    },
    curOtp: {
        type: String,
        required: false
    }

})

UserSchema.pre('save', function (next) {
    this.updated_at = new Date();
    let curRecord = this;
    validator.hashPassword(curRecord.password, (err, hashedPwd) => {
        if(err) {
            console.log('Error while hashing password');
            curRecord.password = curRecord.password;
        } else if(hashedPwd) {
            curRecord.password = hashedPwd;            
        } else {
            curRecord.password = curRecord.password;
        }
        next();
    })
})
UserSchema.pre('update', function (next) {
    this.updated_at = new Date();
    next();
})

const User = mongoose.model('users', UserSchema);

module.exports = {
    UserModel: User,
    UserSchema: UserSchema
}
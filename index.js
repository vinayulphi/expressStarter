// Environment Variables
const result = require('dotenv').config();
const express = require('express');
// Helmet is required for securing the request response headers
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const pinoExpress = require('express-pino-logger')();
const passport = require('passport');
const mongoose = require('mongoose');
const db = mongoose.connect(process.env.DB_URL, { useMongoClient: true}, function(err, result){
    if(err) {
        console.log('Error while connecting to DB:', err);
    } else {
        console.log('The DB Connected');
    }
})

// Initializing Express
const app = express();
// Middleware is enabled to use helmet default options
app.use(helmet());

//Configuring app
app.use(bodyParser.json({
    limit: '50mb'
}));

//Port Number
const port = process.env.PORT || 3000;

//CORS Middleware
app.use(cors());


//Logging Format
morgan.format('combined', '(:date[clf]) -> ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms');
app.use(morgan('combined')); 


//Body Parser Middleware
app.use(bodyParser.json());

//Initializing Passport
app.use(passport.initialize());
app.use(passport.session());
require('./api/config/passport')(passport);
// Configuration and middlewares Ends Here

// Routes

const Auth = require('./api/features/authentication/authentication.routes');
const User = require('./api/features/user/user.routes');

// Index Route
app.get('/v1', (req, res) => {
    res.send('Welcome!!!!');
});
// Public
app.use('/v1/authenticate', Auth);

// Protected
app.use('/v1/user/', passport.authenticate('jwt', {
    session: false
}), User);

//Start Server
app.listen(port, function () {
    console.log('Server Listening on: ' + port);
});
